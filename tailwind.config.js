// tailwind.config.js
// See more information about this file at https://tailwindcss.com/docs/installation#create-your-configuration-file

module.exports = {
  // prefix: 'tw-',
  important: true,
  mode: "jit",
  purge: {
    mode: "all",
    enabled: true,
    content: [
      "./src/**/*.js",
      "./templates/**/*.twig",
      "./templates/**/*.html",
      "./templates/*.html",
      "./templates/*.twig",
    ],
    options: {
      keyframes: true,
      fontFace: true,
    },
  },
  darkMode: "media", // See https://tailwindcss.com/docs/dark-mode
  theme: {
    fontSize: {},
    extend: {
      outline: {
        gold: ["1px solid #D6A249", "0px"],
      },
      flex: {
        2: 2,
        2.5: 2.5,
      },
      zIndex: {
        "-1": -1,
      },
      width: {},
      fontFamily: {},
      colors: {
        pink: "#E3AFB2",
        text: "#656565",
        brown: "#4B473B",
        grey: {
          DEFAULT: "#74787E",
          light: "#D9E5E6",
        },
        yellow: "#F8CD37",
        blue: {
          DEFAULT: "#38805E",
          dark: "#263238",
          darkBg: "#41474F",
          light: "#7BCADF",
          gradient: "#D3F4F0",
          gradient2: "#F6FDFC",
        },
        green: {
          DEFAULT: "#38805E",
          light: "#E8F9EC",
          bg: "#E5F8EA",
          text: "#92E3A9",
        },
      },
    },
  },
  variants: {},
  plugins: [require("flowbite/plugin")],
};
